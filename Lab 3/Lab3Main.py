'''!@file           Lab3Main.py
    @brief          Calls functions in order to create a working encoder data
                    collector and motor controller.
    @details        Periodically calls taskUser, taskEncoder, and taskMotor to 
                    work together using shared variables and generator functions.
                    These shared variables are things like position values or
                    duty cycles that should be transmitted from user input to
                    their designated operating task. The main purpose of 
                    this file is to continuously try to run each task over
                    and over again. The internal task periods will take care
                    of when they should actually run. To stop the program press
                    'Ctrl+C' and to reset the Nucleo press 'Ctrl+D'. For a task
                    diagram, reference the following image:
                        
                    @image html Lab3TD.JPG
                    
    @author         Sean Wahl
    @author         Grant Gabrielson
    @date           February 15, 2022
'''
import shares, taskUser2, taskEncoder2, taskMotor

## @brief Shared variable to represent when the encoder should be zeroed.
#
zFlag = shares.Share(False)

## @brief Shared variable to store the current position of the encoder.
#
position = shares.Share(0)

## @brief Shared variable to store the last delta of the encoder.
#
delta = shares.Share(0)

## @brief Shared variable to keep track of time taken in taskEncoder.
#
timeVal = shares.Share(0)

## @brief Shared variable to store the last angular velocity of the motor.
#
velocity = shares.Share(0)

## @brief Shared variable to set the duty cycle for motor 1 in taskMotor from taskUser.
#
DC1 = shares.Share(0)

## @brief Shared variable to set the duty cycle for motor 2 in taskMotor from taskUser.
#
DC2 = shares.Share(0)

## @brief Shared variable to represent when the motor driver is called to be reset by the user.
#
cFlag = shares.Share(False)

## @brief Shared variable to indicate when the motor has triggered a fault.
#
err = shares.Share(False)

if __name__ == '__main__':
    ## @brief List containing taskUser, taskEncoder, and taskMotor functions.
    #
    tasklist = [taskUser2.taskUserFunction('Task User', 10_000, zFlag, position, delta, timeVal, velocity, DC1, DC2, cFlag, err),
                taskEncoder2.taskEncoderFunction('Task Encoder', 10_000, zFlag, position, delta, timeVal, velocity),
                taskMotor.taskMotorFunction('Task Motor', 10_000, DC1, DC2, cFlag, err)]
    
    while True:
        try:
            for task in tasklist:
                next(task)
        except KeyboardInterrupt:
            break