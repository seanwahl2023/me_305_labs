'''! @page HW0x03       Ball-Balancing Platform Simulation
    @section sec_gen    Overview
                        For Homework 0x03, we were tasked with using our equations
                        of motion that we derived in Homework 0x02 and manipulating
                        them to be in a form that we can easily simulate. The steps
                        we used to do so are as follows based on the homework
                        assignment:
                            
                            1. Decouple the system of equations by multiplying
                               M^-1*f
                            2. Convert the system of two second-order ODEs to
                               a system of four first-order ODEs
                            3. Determine equilibrium points at which the system
                               will remain stationary
                            4. Find the two Jacobian matrices for the new system
                               based on the new state vector and the input torque
                            5. Calculate matrices A and B by evaluating the Jacobians
                               at the equilibrium points
                               
                        After completing this, we had equations of motion representing
                        x, xdot, theta, and thetadot for a given torque meaning 
                        we could create an ODE in MATLAB that we can simulate 
                        when given a time span and initial conditions.
                        
    @section sec_res    Simulation Results
                        For the simulation, we wanted to test two cases:
                            1. The ball is in the center of the platform and
                               the platform is horizontal. Everything is at rest.
                               There is no input torque.
                            2. The ball is 5cm off center of the platform, but the
                               platform is still horizontal. Everything begins at
                               rest. There is no input torque.
                               
                        Although these input conditions only differ with the
                        initial x-position, they produce drastically different
                        results.
                        
                        For the first case, the response looks as so:
                            @image html HW3Sim1.JPG
                        This isn't very interesting but it is exactly what we
                        expect. With the ball in the center of the platform and
                        everything at rest with no input force, the system is
                        in equilibrium. No torques are exerted on the platform
                        by the motor or ball so it has no cause to rotate. Without
                        the platform rotating it remains perfectly horizontal,
                        so the ball remains stationary as well.
                        
                        The second case is slightly more interesting and the response
                        is as follows:
                            @image html HW3Sim2.JPG
                        This looks a little confusing at first but makes sense
                        when thought is put into it. With the ball off center,
                        there is a moment created about the platform pivot,
                        causing the platform's rotation to accelerate as it's
                        tipping. This is why theta and theta dot are only
                        increasing in the response. Now x and xdot at first
                        tend towards the center of the platform, which doesn't
                        make entire intuitive sense. However, since the platform
                        starts rotating instantly there is a moment where the balls
                        inertia keeps it in place and the platform's rotation 
                        causes a relative movement toward the center for the ball.
                        After this moment, the ball moves and acclerates towards
                        the edge as we expect.
                        
                        The simulation results give reassurance that our
                        dynamic model is accurate and makes sense. We can start
                        applying torques to the platform and predict the system
                        response. In doing so we can balance a ball on the
                        platform, as that was our goal in developing this model.
                        
                        
    @section sec_MAT    MATLAB Code
                        Here is the fully documented MATLAB script used to
                        simulate the system.
                        \htmlonly
                        <center> <embed src="HW0x03.pdf" width="850px" height="1000px" href="HW0x03.pdf"></embed> </center>
                        \endhtmlonly
                        
    @author             Sean Wahl
    @date               February 16, 2022
'''