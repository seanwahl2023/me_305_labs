'''!@file           TPMain.py
    @brief          Calls functions in order to create a working encoder data
                    collector and motor controller.
    @details        Periodically calls taskUser, taskBNO, taskMotor, taskClosedLoop,
                    taskPanel, and taskData to 
                    work together using shared variables and generator functions.
                    These shared variables are things like position values or
                    duty cycles that should be transmitted from user input to
                    their designated operating task. The main purpose of 
                    this file is to continuously try to run each task over
                    and over again. The internal task periods will take care
                    of when they should actually run. To stop the program press
                    'Ctrl+C' and to reset the Nucleo press 'Ctrl+D'. 
                    
                    For a task diagram, reference the following image:
                        
                    @image html TPTD.JPG
                    
    @author         Sean Wahl
    @author         Grant Gabrielson
    @author         Spencer Alonge
    @date           March 16, 2022
'''
import shares, taskUserTP, taskBNOTP, taskMotorTP, taskClosedLoopTP, taskPanel, taskData

## @brief Shared variable to store the angular velocities of the platform.
#
velocity = shares.Share(0)

## @brief Shared variable to store the euler angles of the platform.
#
euler = shares.Share(0)

## @brief Shared variable to set the duty cycle of motor 1 in the closed loop control mode.
#
L1 = shares.Share(0)

## @brief Shared variable to set the duty cycle of motor 2 in the closed loop control mode.
#
L2 = shares.Share(0)

## @brief Shared variable holding the closed inner loop proportional gain, acting on the angles of the platform.      
#
Kpthy = shares.Share(10)

## @brief Shared variable holding the closed inner loop derivative gain, acting on the angular velocities of the platform.
#
Kdthy = shares.Share(.25)

## @brief Shared variable holding the closed outer loop proportional gain, acting on the position of the ball. 
#
Kpx = shares.Share(.09)

## @brief Shared variable holding the closed outer loop derivative gain, acting on the velocity of the ball.  
#
Kdx = shares.Share(.05)

## @brief Shared variable to hold the x-axis angular velocity of the platform.
#
wMeas1 = shares.Share(0)

## @brief Shared variable to hold the y-axis angular velocity of the platform.
#
wMeas2 = shares.Share(0)

## @brief Shared variable to hold the x-axis platform angle.
#
thMeas1 = shares.Share(0)

## @brief Shared variable to hold the y-axis platform angle.
#
thMeas2 = shares.Share(0)

## @brief Shared variable to designate when the closed loop control mode should be active.
#
wFlag = shares.Share(False)

## @brief Shared variable to hold the calibration status of the IMU.
#
cal_state = shares.Share([0, 0, 0, 0])

## @brief Shared variable to designate when the IMU has been calibrated.
#
calFlag = shares.Share(False)

## @brief Shared variable to designate when the IMU or touch panel have calibrated off a file.
#
fileFlag = shares.Share(False)

## @brief Shared variable holding the ball's x-position.
#
x = shares.Share(0)

## @brief Shared variable holding the ball's y-position.
#
y = shares.Share(0)

## @brief Shared variable holding the ball's x-velocity.
#
xdot = shares.Share(1)

## @brief Shared variable holding the ball's y-velocity.
#
ydot = shares.Share(.1)

## @brief Shared variable representing when the ball is contacting the touch panel.
contact = shares.Share(False)

## @brief Shared variable holding the touch panel calibration coefficients.
betamat = shares.Share(0)

## @brief Shared variable indicating when the user wants the touch panel to
#         be calibrated.
#
touchFlag = shares.Share(False)

## @brief Shared variable indicating x-related data should be taken.
#
xdataFlag = shares.Share(False)

## @brief Shared variable indicating y-related data should be taken.
#
ydataFlag = shares.Share(False)

## @brief Shared variable indicating that x and y position data should be taken.
#
posdataFlag = shares.Share(False)

## @brief Shared variable indicating when data is done being written to a file.
#
doneFlag = shares.Share(False)

## @brief Shared variable indicating when data is done being collected.
#
collectionFlag = shares.Share(False)

## @brief Shared variable holding the duty cycle offset needed to make up for motor bias.
#
xgain = shares.Share(0)

## @brief Shared variable holding the duty cycle offset needed to make up for motor bias. 
#
ygain = shares.Share(0)

if __name__ == '__main__':
    ## @brief List containing taskUser, taskBNO, taskMotor, taskClosedLoop, taskPanel, and taskData functions.
    #
    tasklist = [taskUserTP.taskUserFunction('Task User', 50_000, euler, velocity, L1, L2, wFlag, cal_state, calFlag, fileFlag, x, y, xdot, ydot, contact, betamat, touchFlag, Kpthy, Kdthy, Kpx, Kdx, xdataFlag, ydataFlag, posdataFlag, doneFlag, collectionFlag, xgain, ygain),
                taskBNOTP.taskBNOFunction('Task BNO', 5_000, cal_state, velocity, euler, calFlag, wMeas1, wMeas2, thMeas1, thMeas2, fileFlag),
                taskMotorTP.taskMotorFunction('Task Motor', 5_000, wFlag, L1, L2),
                taskClosedLoopTP.taskClosedLoopFunction('Task Closed Loop Controller', 10_000, L1, L2, wMeas1, wMeas2, thMeas1, thMeas2, x, y, xdot, ydot, Kpthy, Kdthy, Kpx, Kdx, xgain, ygain),
                taskPanel.taskPanelFunction('Task Panel', 5_000, x, y, xdot, ydot, contact, touchFlag, betamat, fileFlag),
                taskData.taskDataFunction('Task Data', 5_000, xdataFlag, ydataFlag, posdataFlag, x, xdot, y, ydot, thMeas1, wMeas1, thMeas2, wMeas2, collectionFlag, doneFlag)]
    
    while True:
        try:
            for task in tasklist:
                next(task)
        except KeyboardInterrupt:
            break