'''!@file           Lab_0x01.py
    @brief          This script takes user input through a button to control LED patterns.      
    @details        By implementing non-blocking code and interrupting the code
                    upon a button press, this code is able to control LED patterns
                    and switch upon pressing said button. The code will start in
                    an idle state, displaying a welcome message and waiting for
                    an initial button press. When the button is pressed it will
                    begin with a square wave brightness pattern for the LED. On
                    the next press the LED will begin a sine wave pattern. Finally,
                    on the third press the LED will begin a saw wave pattern.
                    This cycle order will repeat (excluding the idle state)
                    upon subsequent button presses. At any point the user can
                    press the button in order to switch patterns, and the LED will
                    continue the pattern indefinitely until the next button press.
                    If the user is done pressing the button and would like to
                    stop the script, they should press "Ctrl+C" to stop the code
                    and the "Ctrl+D" to reset the Nucleo. 
                    
                    Source code is available at: https://bitbucket.org/seanwahl2023/me_305_labs/src/master/Lab%201/
                    
                    A video demonstration can be found at: https://youtu.be/VKgwzVVg67s
                   
                    For state transition diagrams and the task diagram please 
                    reference the following image:
                    @image html Lab1States.JPG
                    
    @author Grant Gabrielson
    @author Sean Wahl
    @date   January 19, 2022
'''

import time
import math
import pyb

def onButtonPress(IRQ_src):
    '''!@brief          Detects interrupt request through button press.
        @details        When the button is pressed, this function will set
                        a variable as true, indicating that the button has been
                        pressed and allowing the script to enact on the button
                        press elsewhere.
        @param IRQ_src  Interrupt service request notation. 
    '''
    global buttonPressed
    buttonPressed = True

def SawWave(t):
    '''!@brief      Creates a sawtooth wave.
        @details    Takes in a time value and will output the corresponding
                    saw wave value at that time. The saw wave has a period of
                    one second and a max height of 100.
        @param t    Time relative to a starting point.
        @return     Saw wave value at t.
    '''
    return (t%1000)/10

def SquareWave(t):
    '''!@brief      Creates a square wave.
        @details    Takes in a time value and will output the corresponding
                    square wave value at that time. The square wave has a
                    period of one second and a height of 0 or 100.
        @param t    Time relative to a starting point.
        @return     Square wave value at t.
    '''
    compare = (t%1000)
    if compare < 500:
        return 100
    else:
        return 0

def SineWave(t):
    '''!@brief      Creates a sine wave.
        @details    Takes in a time value and will output the corresponding
                    sine wave value at that time. The sine wave has a period of
                    ten seconds and an amplitude of 50, with a y-intercept of
                    50.
        @param t    Time relative to a starting point.
        @return     Sine wave value at t.
    '''
    return 50*math.sin(3.1415*t/(1000*5))+50
   
if __name__ == '__main__':
    print('Welcome! Press button B1 to change state.')
    print('In state 0. Waiting for button press.')
    
    ## Pin object registering pin C13 on the Nucleo. This pin is for the button.
    #
    pinC13 = pyb.Pin (pyb.Pin.cpu.C13)
    
    ## Setup for an interrupt request from the falling edge of Pin C13.
    #
    ButtonInt = pyb.ExtInt(pinC13,mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=onButtonPress)
    
    ## @brief       Global variable that indicates when the button is pressed.
    #  @details     This variable denotes whether or not the button has been
    #               pressed. It will be changed to true on the falling edge of
    #               a button press, and should be set back to false immediately
    #               following this action.
    #
    buttonPressed = False
    
    ## @brief       Denotes what LED pattern the script is currently running.
    #  @details     The system has four possible states. The idle state when
    #               initially waiting for a button press is 0. The square
    #               wave state is 1. The sine wave state is 2, and the saw wave
    #               state is 3. When the buttonPressed variable is True, it 
    #               triggers a state transition to the next logical state.
    #               The state will always start at 0 and cycle through 1, 2,
    #               and 3 in that order indefinitely.
    #
    state = 0
    
    ## Pin object registering pin A5 on the Nucleo. This pin is for the LED.
    #
    pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
    
    ## Sets a timer for the LED frequency.
    #
    tim2 = pyb.Timer(2, freq=20000)
    
    ## Sets a channel to communicate with the LED.
    #
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin = pinA5)
   
    while True:
        try:
            #code for FSM
           
            if state == 0:
                if buttonPressed == 1:
                    state = 1
                    
                    ## Sets a reference time when transitioning between states.
                    #
                    t = time.ticks_ms()
                    buttonPressed = False
                    print('Square wave pattern selected.')
            elif state == 1:
                
                ## Controls the LED brightness 
                #
                brt = SquareWave(time.ticks_diff(time.ticks_ms(), t))
                t2ch1.pulse_width_percent(brt)
                if buttonPressed:
                    buttonPressed = False
                    state = 2
                    t = time.ticks_ms()
                    print('Sine wave pattern selected.')
            elif state == 2:
                brt = SineWave(time.ticks_diff(time.ticks_ms(), t))
                t2ch1.pulse_width_percent(brt)
                if buttonPressed:
                    buttonPressed = False
                    state = 3
                    print('Saw wave pattern selected.')
                   
            elif state == 3:
                brt = SawWave(time.ticks_diff(time.ticks_ms(), t))
                t2ch1.pulse_width_percent(brt)
                if buttonPressed:
                    buttonPressed = False
                    state = 1
                    print('Square wave pattern selected.')
                   
            pass
        except KeyboardInterrupt:
                break
       
    print('Program Terminating')