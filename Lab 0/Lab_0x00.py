# -*- coding: utf-8 -*-
'''@file Lab_0x00.py
Created on Thu Jan  6 08:44:23 2022

@author: seanw
'''

def fib (idx):
    '''
    @brief      This function automatically calculates a Fibonacci number 
                at a specific index.
    @param idx  A whole number (positive integer and 0) specifying the index 
                of the desired Fibonacci number
    '''

    #This if not statement is a last check to make sure the index inputted is
    #a whole number. It doesn't do any major error formatting, just a basic 
    #'None' output if the number inputted isn't a whole number.
    
    if not str(idx).isdigit():
        return print(None)
    
    #These two elif statements now will output results for indices 0 and 1.
    #There are other ways to do this but, this just check if the number input 
    #is 0 or 1, and then set the output to the appropriate value if met.
    
    elif int(idx) == 0:
        f_n = 0
    elif int(idx) == 1:
        f_n = 1
    
    #If this point is reached, the index is valid and greater than 1, so now
    #the code will carry out the Fibonacci calculations as necessary.
    
    else:
        
    #First, an initial list containing the first two Fibonacci numbers is 
    #created. An arbitrary variable is also predefined as 2, as this is the
    #first index that is needed to be calculated off of the list. The while
    #loop will expand the list until the desired index is reached, and when it
    #ends, the output will be defined as the appropriate Fibonacci number.
    #The last line of the loop steps the loop forward so that code will keep
    #calculating the next index.
    
        my_mat = [0,1]
        n=2
    
        while n <= int(idx):
            f_n = my_mat[n-1] + my_mat[n-2]
            my_mat.append(f_n)
            n = n+1
            
    #Returns the output variable as the final result.
        
    return f_n

#This section will only run when the script is executed as its own file.
#This section will not run when imported, only returning the Fibonacci
#number as the result of the function.

if __name__ == '__main__':
    
    #Prints an introduction to the user interface. Set a dummy variable as 
    #True, so now a loop can be run while that variable is still True.
    
    print('Welcome to the Fibonacci calculator!')
    on = True
    
    #While loop will continue to prompt the user for an index until an
    #appropriate index is input. Once a proper index is received, an output 
    #of the user's Fibonacci number is printed. Afterwards, the user is asked
    #is they would like to continue or quit. If the correct button, q, is
    #input, then the code will end as the dummy variable will be set to False. 
    #If not, the code will ask for another Fibonacci index.
    
    while on == True:
        idx = input('Please enter a whole number: ')
        while not str(idx).isdigit():
            print('{:} is not a whole number! Please try again.'.format(idx))
            idx = input('Please enter a whole number: ')
        print('Fibonacci number at index {:} is {:}.'.format(idx,fib(idx)))
        if (input('Press enter to continue, or q to quit: ') == 'q'):
            on = False