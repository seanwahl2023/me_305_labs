# -*- coding: utf-8 -*-
"""
Created on Thu Jan  6 08:34:48 2022

@author: seanw
"""

def my_function():
    my_string = input('Please enter a string: ')
    if my_string == 'foo':
        print('bar')
    else:
        print('Not valid')
    
if __name__ == '__main__':
    my_function()